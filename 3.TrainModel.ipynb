{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Training the model\n",
    "\n",
    "Using recorded images from webcam to fine-tune the VGG model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from keras.applications.vgg16 import VGG16\n",
    "from keras.applications.vgg16 import preprocess_input\n",
    "from keras.preprocessing import image\n",
    "from keras.utils import plot_model\n",
    "from keras import models\n",
    "from keras import layers\n",
    "from keras import optimizers\n",
    "from keras.wrappers.scikit_learn import KerasClassifier\n",
    "from keras.preprocessing.image import img_to_array\n",
    "import keras.utils\n",
    "from sklearn.model_selection import KFold\n",
    "from sklearn.model_selection import cross_val_score\n",
    "import sklearn.preprocessing\n",
    "import sklearn.model_selection\n",
    "import numpy as np\n",
    "import os\n",
    "import cv2\n",
    "import h5py\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the pre-trained model\n",
    "\n",
    "The first time you instanciate the VGG16 model, Keras will download the weight files from the Internet and store them in the ~/.keras/models directory.\n",
    "\n",
    "**Note** that the weights are about 528 megabytes, so the download may take a few minutes depending on the speed of your Internet connection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "image_size=48 # number of pixels of our data (actually image_size x image_size)\n",
    "\n",
    "# include_top=False means that we only take the convolutional layers and do not include the top (classification) layers.\n",
    "pre_model = VGG16(include_top=False, weights='imagenet', input_shape=(image_size, image_size, 3))\n",
    "\n",
    "# Freezes the weights of the pre-trained model\n",
    "for layer in pre_model.layers:\n",
    "    layer.trainable = False \n",
    "\n",
    "# details of the VGG16 network\n",
    "pre_model.summary() # notice the number of trainable parameters at the bottom"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Add custom classification layers\n",
    "\n",
    "Her we add our own classification layers on top of the pre-trained VGG16 network.  \n",
    "\n",
    "> For multiclass, single-label classification (more than 2 people) use `activation='softmax'`.  \n",
    "> For binary classification (2 people) use `activation='softmax'`.  \n",
    "> The loss function has to be adaptet accordingly, see note below.\n",
    "\n",
    "Choosing the right last-layer activation and loss function for your model:  \n",
    "\n",
    "| Problem type                            | Last-layer activation | Loss function      |\n",
    "| :-------------------------------------- |:--------------------- | :----------------- |\n",
    "| Binary classification                   | `sigmoid` | `binary_crossentropy`          |\n",
    "| Multiclass, single-label classification | `softmax` | `categorical_crossentropy`     |\n",
    "| Multiclass, multilabel classification   | `sigmoid` | `binary_crossentropy`          |\n",
    "| Regression to arbitrary values          | None      | `mse`                          |\n",
    "| Regression to values between 0 and 1    | `sigmoid` | `mse` or `binary_crossentropy` |\n",
    "\n",
    "Source: Chollet, F. (2017). Deep Learning with Python. Manning Publications"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read already known labels\n",
    "with open('IDlog.txt', 'r') as f:\n",
    "    known_faces=f.read().splitlines()\n",
    "\n",
    "# Stores number of known faces ( = number of nodes in top layer because of one-hot encoding)\n",
    "num_faces=len(known_faces)\n",
    "\n",
    "\n",
    "# Create the model\n",
    "model = models.Sequential()\n",
    " \n",
    "# Add the pre-trained model\n",
    "model.add(pre_model)\n",
    "\n",
    "\n",
    "# Add new layers for classification\n",
    "model.add(layers.Flatten())\n",
    "model.add(layers.Dense(1024, activation='relu'))\n",
    "model.add(layers.Dropout(0.5))\n",
    "model.add(layers.Dense(512, activation='relu'))\n",
    "model.add(layers.Dropout(0.5))\n",
    "model.add(layers.Dense(num_faces, activation='softmax')) # binary or multiclass (>2 people)?\n",
    " \n",
    "# Show a summary of the model. \n",
    "model.summary()\n",
    "\n",
    "# extra library neede for plotting the model (conda install pydot)\n",
    "#plot_model(model, to_file='model_plot.png', show_shapes=True, show_layer_names=True) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compiling the model\n",
    "\n",
    "> For multiclass classification problems `categorical_crossentropy` is a good loss function.  \n",
    "> For two people use `binary_crossentropy` instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model.compile(loss='categorical_crossentropy', # binary or multiclass (>2 people)?\n",
    "              optimizer='adam', \n",
    "              metrics=['accuracy']) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Load the data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "known_faces"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "VGG16 uses \"caffe\" style, that is not normalized, but is centered. (\"The only preprocessing we do is subtracting the mean RGB value, computed on the training set, from each pixel.\" - [Very Deep Convolutional Networks for Large-Scale Image Recognition](https://arxiv.org/abs/1409.1556), 2014)  \n",
    "Keras provides a function called `preprocess_input()` to prepare new input for the network accordingly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "trainingImages = []\n",
    "trainingLabels = []\n",
    "\n",
    "# Get the pictures (data) in ./dataSet/\n",
    "for filename in os.listdir('dataSet/'):\n",
    "    img = cv2.imread(os.path.join('dataSet/',filename))\n",
    "    img = cv2.resize(img, (image_size, image_size)) \n",
    "    img = img_to_array(img) # Convert image into an array that keras can use    \n",
    "    img = preprocess_input(img) # prepare the image for the VGG model\n",
    "    trainingImages.append(img)\n",
    "    \n",
    "     \n",
    "    label = filename[0] # the numeric label is the first part of the file name\n",
    "    trainingLabels.append(label)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Reshaping data for training\n",
    "trainingImages = np.array(trainingImages, dtype='float') # convert to numpy array\n",
    "trainingLabels = np.array(trainingLabels, dtype='int') # convert to numpy array\n",
    "\n",
    "# Initialize LabelEncoder\n",
    "le = sklearn.preprocessing.LabelEncoder().fit(trainingLabels) \n",
    "\n",
    "# Convert into a binary class matrix, ie one hot encoded\n",
    "trainingLabels = keras.utils.to_categorical(le.transform(trainingLabels), num_faces) \n",
    "\n",
    "# Uncoment following to see the how the labels are encoded\n",
    "#print(trainingLabels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Split the data into training and testing sets, default is 80% train and 20% test\n",
    "(x_train, x_test, y_train, y_test) = sklearn.model_selection.train_test_split(trainingImages, \n",
    "                                                                              trainingLabels, test_size=0.20) \n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting the model"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fitted_model = model.fit(x_train, y_train, validation_data=(x_test, y_test), \n",
    "                         batch_size=20, epochs=25, verbose=1, shuffle=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Saving the model\n",
    "model.save('FaceCNN.h5py')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "## Displaying curves of loss and accuracy during training"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "acc = fitted_model.history['acc']\n",
    "val_acc = fitted_model.history['val_acc']\n",
    "loss = fitted_model.history['loss']\n",
    "val_loss = fitted_model.history['val_loss']\n",
    "epochs = range(1, len(acc) + 1)\n",
    "plt.plot(epochs, acc, 'bo', label='Training acc')\n",
    "plt.plot(epochs, val_acc, 'b', label='Validation acc')\n",
    "plt.title('Training and validation accuracy')\n",
    "plt.legend()\n",
    "plt.figure()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(epochs, loss, 'bo', label='Training loss')\n",
    "plt.plot(epochs, val_loss, 'b', label='Validation loss')\n",
    "plt.title('Training and validation loss')\n",
    "plt.legend()\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
